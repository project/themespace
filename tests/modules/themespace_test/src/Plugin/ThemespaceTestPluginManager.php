<?php

namespace Drupal\themespace_test\Plugin;

use Drupal\themespace\Plugin\Discovery\ProviderTypedAnnotatedClassDiscovery;
use Drupal\themespace\Plugin\Discovery\ProviderTypedYamlDiscoveryDecorator;
use Drupal\themespace\Plugin\ProviderTypedPluginManagerInterface;
use Drupal\themespace\Plugin\ProviderTypedPluginManager;

/**
 * Test plugin manager to test the ProviderTypedPluginManagerTrait.
 */
class ThemespaceTestPluginManager extends ProviderTypedPluginManager implements ProviderTypedPluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new ProviderTypedAnnotatedClassDiscovery(
        $this->subdir,
        $this->namespaces,
        $this->pluginDefinitionAnnotationName,
        $this->additionalAnnotationNamespaces
      );
      $this->discovery = new ProviderTypedYamlDiscoveryDecorator(
        $discovery,
        'themespace_test',
        $this->moduleHandler->getModuleDirectories(),
        $this->themeHandler->getThemeDirectories()
      );
    }
    return $this->discovery;
  }

}
