<?php

namespace Drupal\themespace_test\Plugin\Themespace;

use Drupal\Core\Plugin\PluginBase;

/**
 * A basic plugin implementation from the test module.
 *
 * @ThemespaceTest(
 *   id = "module.test.annotated",
 *   label = @Translation("Annotated module"),
 * )
 */
class ModuleAnnoPlugin extends PluginBase {

}
