<?php

namespace Drupal\themespace_test\Plugin\Themespace;

use Drupal\Core\Plugin\PluginBase;

/**
 * A basic plugin implementation used in the YAML discovery.
 */
class ModuleYamlPlugin extends PluginBase {

}
