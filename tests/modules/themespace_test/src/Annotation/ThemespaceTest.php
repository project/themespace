<?php

namespace Drupal\themespace_test\Annotation;

use Drupal\themespace\Annotation\ProviderTypedPlugin;
use Drupal\themespace_test\Plugin\TestPluginDefinition;

/**
 * Annotation for definining annotated plugins.
 *
 * @Annotation
 */
class ThemespaceTest extends ProviderTypedPlugin {

  /**
   * The plugin identifier.
   *
   * @var string
   */
  public $id;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new TestPluginDefinition($this->definition);
  }

}
