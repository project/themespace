<?php

namespace Drupal\Theme\themespace_test_subtheme\Plugin\Themespace;

use Drupal\Core\Plugin\PluginBase;

/**
 * A basic plugin implementation from the test theme.
 *
 * @ThemespaceTest(
 *   id = "subtheme.test.annotated",
 * )
 */
class SubthemeAnnoPlugin extends PluginBase {

}
