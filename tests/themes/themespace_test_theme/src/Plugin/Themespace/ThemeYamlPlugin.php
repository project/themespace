<?php

namespace Drupal\Theme\themespace_test_theme\Plugin\Themespace;

use Drupal\Core\Plugin\PluginBase;

/**
 * A basic plugin implementation used in the YAML discovery.
 */
class ThemeYamlPlugin extends PluginBase {

}
