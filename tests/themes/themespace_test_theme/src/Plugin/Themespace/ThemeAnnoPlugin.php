<?php

namespace Drupal\Theme\themespace_test_theme\Plugin\Themespace;

use Drupal\Core\Plugin\PluginBase;

/**
 * A basic plugin implementation from the test theme.
 *
 * @ThemespaceTest(
 *   id = "theme.test.annotated",
 *   label = @Translation("Theme test"),
 * )
 */
class ThemeAnnoPlugin extends PluginBase {

}
