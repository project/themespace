<?php

namespace Drupal\themespace\Plugin\Discovery;

use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\Component\Annotation\Plugin\Discovery\AnnotatedClassDiscovery as ComponentAnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\themespace\Annotation\ProviderTypedPluginInterface;

/**
 * Annotated class discovery for plugins which are aware of the provider type.
 *
 * A plugin discovery handler for annotated plugins which are aware of their
 * provider's extension type. For theme provided plugins, is it important
 * for plugin managers and consumers to be aware these are provided by themes
 * and should only be used when that theme is active.
 *
 * @see \Drupal\themespace\Annotation\ProviderTypedPluginInterface
 */
class ProviderTypedAnnotatedClassDiscovery extends AnnotatedClassDiscovery implements ProviderTypedDiscoveryInterface {

  /**
   * The full root namespaces to traverse for discovery.
   *
   * This should never be altered, and is used to build module or theme
   * iterators from.
   *
   * @var \Traversable
   *
   * @see \Drupal\themespace\Discovery\ProviderTypedAnnotatedClassDiscovery::getDefinitions()
   * @see \Drupal\themespace\Discovery\ProviderTypedAnnotatedClassDiscovery::getModuleDefinitions()
   * @see \Drupal\themespace\Discovery\ProviderTypedAnnotatedClassDiscovery::getThemeDefinitions()
   */
  protected $fullNamespaceIterator;

  /**
   * Creates a new instance of the ProviderTypedAnnotatedClassDiscovery class.
   *
   * @param string $subdir
   *   Either the plugin's subdirectory, for example 'Plugin/views/filter', or
   *   empty string if plugins are located at the top level of the namespace.
   * @param \Traversable $root_namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   *   If $subdir is not an empty string, it will be appended to each namespace.
   * @param string $plugin_annotation_class
   *   (optional) The name of the annotation that contains the plugin
   *   definition. Defaults to 'Drupal\Component\Annotation\Plugin'.
   * @param array $annotation_namespaces
   *   (optional) Additional namespaces to scan for annotation definitions.
   */
  public function __construct($subdir, \Traversable $root_namespaces, $plugin_annotation_class = 'Drupal\\themespace\\Annotation\\ProviderTypedPlugin', array $annotation_namespaces = []) {
    parent::__construct($subdir, $root_namespaces, $plugin_annotation_class, $annotation_namespaces);

    $this->fullNamespaceIterator = $root_namespaces;
  }

  /**
   * Use the namespace of the plugin class to determine the provider and type.
   *
   * Theme namespaces which are added by the themespace module all start with
   * "Drupal\Theme\<theme>" and are assumed to be theme provided plugin.
   * Otherwise the \Drupal\Core\DrupalKernel::getModuleNamespacesPsr4() adds the
   * module and profile namespaces starting with "Drupal\<module>".
   *
   * We can take advantage of these 2 prefixes patterns to get the provider name
   * and guess the extension type of the provider.
   *
   * @param string $namespace
   *   The plugin class namespace or fully namespaced class name.
   *
   * @return string[]
   *   An array with two values, the first will be the provider name, and the
   *   second value in the array is the extension type of the provider. Any
   *   of these values can't be determined, their respective index will have a
   *   value of NULL.
   *
   * @see \Drupal\Core\DrupalKernel::getModuleNamespacePsr4()
   * @see \Drupal\themespace\Compiler\ThemeNamespacesCompilerPass::process()
   */
  protected function getProviderInfoFromNamespace($namespace): array {
    if (preg_match('|^Drupal\\\\(?<theme>Theme\\\\)?(?<provider>[\w]+)\\\\|', $namespace, $matches)) {
      return [
        mb_strtolower($matches['provider']),
        empty($matches['theme']) ? 'module' : 'theme',
      ];
    }

    return [NULL, NULL];
  }

  /**
   * {@inheritdoc}
   */
  protected function getProviderFromNamespace($namespace) {
    return $this->getProviderInfoFromNamespace($namespace)[0];
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareAnnotationDefinition(AnnotationInterface $annotation, $class) {
    ComponentAnnotatedClassDiscovery::prepareAnnotationDefinition($annotation, $class);

    if ($annotation instanceof ProviderTypedPluginInterface) {
      if (!$annotation->getProvider() || !$annotation->getProviderType()) {
        [$provider, $type] = $this->getProviderInfoFromNamespace($class);
        $annotation->setProvider($provider);
        $annotation->setProviderType($type);
      }
    }
    elseif (!$annotation->getProvider()) {
      $annotation->setProvider($this->getProviderFromNamespace($class));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    // Ensure this is reset before attempting to do any discovery.
    $this->rootNamespacesIterator = $this->fullNamespaceIterator;

    return parent::getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDefinitions(): array {
    // Create an iterator that only traverses namespaces that match the Drupal
    // namespace pattern. We alter the $this->rootNamespacesIterator so it can
    // be use in the parent::getDefinitions().
    // This avoids having to reimplement
    // \Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery::getDefinitions().
    $iterator = new \IteratorIterator($this->fullNamespaceIterator);
    $this->rootNamespacesIterator = new \CallbackFilterIterator($iterator, function ($value, $key) {
      return strpos($key, 'Drupal\\') === 0 && strpos($key, 'Theme\\', 7) !== 7;
    });

    return parent::getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getThemeDefinitions(): array {
    // @see \Drupal\themespace\Discovery\ProviderTypedAnnotatedClassDiscovery::getModuleDefinitions()
    $iterator = new \IteratorIterator($this->fullNamespaceIterator);
    $this->rootNamespacesIterator = new \CallbackFilterIterator($iterator, function ($value, $key) {
      return strpos($key, 'Drupal\\Theme\\') === 0;
    });

    return parent::getDefinitions();
  }

}
