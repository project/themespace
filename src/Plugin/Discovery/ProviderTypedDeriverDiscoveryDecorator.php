<?php

namespace Drupal\themespace\Plugin\Discovery;

use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\themespace\Plugin\Definition\MergeablePluginDefinitionInterface;

/**
 * Plugin discovery decorator which adds derivative definitions.
 *
 * This version of the deriver discovery decorator just implements the
 * \Drupal\themespace\Plugin\Discovery\ProviderTypedDiscoveryInterface by adding
 * the ::getModuleDefinitions() and ::getThemeDefinition() methods.
 */
class ProviderTypedDeriverDiscoveryDecorator extends ContainerDerivativeDiscoveryDecorator implements ProviderTypedDiscoveryInterface {

  /**
   * {@inheritdoc}
   */
  protected function mergeDerivativeDefinition($base_plugin_definition, $derivative_definition) {
    if (is_array($base_plugin_definition) && is_array($derivative_definition)) {
      return parent::mergeDerivativeDefinition($base_plugin_definition, $derivative_definition);
    }
    elseif ($derivative_definition instanceof MergeablePluginDefinitionInterface) {
      $derivative_definition->mergeDefinition($base_plugin_definition);
    }

    // If definitions are not arrays and are plugin objects, and there is no
    // clear way to merge them, just return the derivative definition.
    return $derivative_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDefinitions(): array {
    if ($this->decorated instanceof ProviderTypedDiscoveryInterface) {
      $pluginDefs = $this->decorated->getModuleDefinitions();
      return $this->getDerivatives($pluginDefs);
    }

    // If decorated is not provider type aware, treat all plugin definitions
    // as if they are "module" provided definitions, getDefinitions() method.
    return parent::getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getThemeDefinitions(): array {
    if ($this->decorated instanceof ProviderTypedDiscoveryInterface) {
      $pluginDefs = $this->decorated->getThemeDefinitions();
      return $this->getDerivatives($pluginDefs);
    }

    // If decorated discovery is not provider type aware, treat all definitions
    // as if they are "module" provided definitions - so there would be no
    // theme definition to create derivatives for.
    return [];
  }

}
