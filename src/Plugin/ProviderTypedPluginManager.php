<?php

namespace Drupal\themespace\Plugin;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\themespace\Plugin\Discovery\ProviderTypedAnnotatedClassDiscovery;

/**
 * A default plugin manager for supporting the provider typed plugins.
 *
 * Meant to be a replacement for \Drupal\Core\Plugin\DefaultPluginManager when
 * the plugin manager needs to support separation module and theme plugins.
 * Implementers are responsible for determining how to apply their theme plugins
 * based on which theme is active.
 *
 * Some plugins (like preprocessors) can be applied to and cached in the
 * hook_theme_alter() per active theme, and getting the theme plugins with
 * the self::getDefinitionsByTheme() is sufficient. This may not work for some
 * plugin use cases so be careful!
 */
class ProviderTypedPluginManager extends DefaultPluginManager implements ProviderTypedPluginManagerInterface {

  use ProviderTypedPluginManagerTrait;

  /**
   * Create a new instance of the ProviderTypedPluginManager class.
   *
   * @param string|bool $subdir
   *   The plugin's subdirectory, for example Plugin/views/filter.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param string|null $plugin_interface
   *   The interface each plugin should implement.
   * @param string $plugin_definition_annotation_name
   *   The name of the annotation that contains the plugin definition. Defaults
   *   to 'Drupal\themespace\Annotation\ProviderTypedPlugin'.
   * @param string[] $additional_annotation_namespaces
   *   Additional namespaces to scan for annotation definitions.
   */
  public function __construct($subdir, \Traversable $namespaces, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, $plugin_interface = NULL, $plugin_definition_annotation_name = 'Drupal\\themespace\\Annotation\\ProviderTypedPlugin', array $additional_annotation_namespaces = []) {
    parent::__construct(
      $subdir,
      $namespaces,
      $module_handler,
      $plugin_interface,
      $plugin_definition_annotation_name,
      $additional_annotation_namespaces
    );

    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    parent::clearCachedDefinitions();

    // If definition caches have been cleared, also remove our currently
    // grouped plugin definitions.
    unset($this->moduleDefinitions);
    unset($this->themeDefinitions);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new ProviderTypedAnnotatedClassDiscovery(
        $this->subdir,
        $this->namespaces,
        $this->pluginDefinitionAnnotationName,
        $this->additionalAnnotationNamespaces
      );
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
    }
    return $this->discovery;
  }

}
