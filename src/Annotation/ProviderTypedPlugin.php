<?php

namespace Drupal\themespace\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Base plugin annotation for ProviderTypedPlugin classes.
 *
 * @see \Drupal\themespace\Annotation\ProviderTypedPluginInterface
 */
class ProviderTypedPlugin extends Plugin implements ProviderTypedPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getProviderType() {
    if (!empty($this->definition['provider_type'])) {
      return $this->definition['provider_type'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setProviderType($extension_type): void {
    if ($extension_type) {
      $this->definition['provider_type'] = $extension_type;
    }
    else {
      unset($this->definition['provider_type']);
    }
  }

}
